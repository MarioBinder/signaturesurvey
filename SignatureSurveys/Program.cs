﻿using System;

namespace SignatureSurveys
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("type your path: (press CTRL-V) ");

            ConsoleKeyInfo ki = Console.ReadKey(true);
            if ((ki.Key == ConsoleKey.V) && (ki.Modifiers == ConsoleModifiers.Control))
            {
                var cb = ClipBoard.PasteTextFromClipboard();

                var path = cb.Remove(cb.Length - 1, 1);

                var survey = new Survey();

                var classes = survey.ReadClasses(path, "cs");
                foreach (var @class in classes)
                {
                    var loc = survey.LinesOfCode(@class);
                    var fileName = survey.GetClassName(@class);
                    Console.WriteLine(string.Format("{0} | {1} | {2}", fileName, loc, survey.GetSignature(@class)));
                }
            }

            Console.ReadLine();
        }
    }
}
